<?php
namespace Dayone\Issuer;

class Card {

    public function __construct(){

    }

    /**
     * @author Ha Tran <manhhaniit@gmail.com>
     */
    public function view()
    {
        \App::register('Dayone\Issuer\CardServiceProvider');
        return 'Card::index';
    }

}