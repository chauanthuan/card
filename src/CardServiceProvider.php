<?php
/**
 * Created by PhpStorm.
 * User: Rika
 * Date: 8/22/2016
 * Time: 10:18 AM
 */
namespace Dayone\Issuer;

use Illuminate\Support\ServiceProvider;

class CardServiceProvider extends ServiceProvider{

    public function boot()
    {   
        // $this->loadViewsFrom(__DIR__.'/Views', 'issue');
       
    }

    public function register()
    {
         $this->loadViewsFrom(__DIR__.'/Views', 'Card');
    }
    
}